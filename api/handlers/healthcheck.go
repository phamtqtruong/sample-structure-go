package handlers

import (
	"backend_with_golang/pkg/rest"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Healthcheck(c *gin.Context) {
	c.JSON(http.StatusOK, rest.Response{
		Status: 1,
		Code:   http.StatusOK,
		Data: gin.H{
			"message": "It works",
		},
	})
}
