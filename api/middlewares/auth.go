package middlewares

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Auth(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusOK, "You must authenticate")
}
