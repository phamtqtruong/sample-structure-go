package api

import (
	"backend_with_golang/api/handlers"
	"backend_with_golang/api/middlewares"
	"backend_with_golang/pkg/rest"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type App struct {
	Router *gin.Engine
	DB     *sql.DB
}

type Database struct {
	Host, Username, Password, DbName string
}

func (a *App) Initialize(db *Database) {
	db.connectDB(a)
	routes := a.initRoutes()
	for _, route := range routes {
		var handlersChain gin.HandlersChain
		if route.HandlersChain != nil {
			handlersChain = route.HandlersChain
		} else {
			handlersChain = gin.HandlersChain{route.HandlerFunc}
		}
		if !route.Public {
			handlersChain = append(gin.HandlersChain{middlewares.Auth}, handlersChain...)
		}
		a.Router.Handle(route.Method, route.Pattern, handlersChain...)
	}
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func (db *Database) connectDB(a *App) {
	if a.DB == nil {
		uri := db.Username + ":" + db.Password + "@" + db.Host + "/" + db.DbName
		fmt.Printf("Connect to database: %v/%v\n", db.Host, db.DbName)
		conn, err := sql.Open("mysql", uri)
		if err != nil {
			panic(err)
		}
		a.DB = conn
		// See "Important settings" section.
		a.DB.SetConnMaxLifetime(time.Minute * 3)
		a.DB.SetMaxOpenConns(10)
		a.DB.SetMaxIdleConns(10)
		fmt.Printf("Database connected.\n")
	}
}

func (a *App) initRoutes() rest.Routes {
	a.Router = gin.New()
	routes := rest.Routes{
		rest.Route{
			Name:        "Index",
			Method:      "GET",
			Pattern:     "/",
			HandlerFunc: handlers.Healthcheck,
			Public:      true,
		},
	}
	return routes
}
