package main

import (
	"log"
	"os"

	"backend_with_golang/api"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	host := os.Getenv("MYSQL_HOST")
	username := os.Getenv("MSQL_USER")
	password := os.Getenv("MYSQL_PASSWORD")
	dbName := os.Getenv("MYSQL_DBNAME")

	app := api.App{}
	app.Initialize(&api.Database{Host: host, Username: username, Password: password, DbName: dbName})
	app.Run(":" + port)
}
